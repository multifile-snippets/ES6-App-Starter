# ES6 app starter pack
This is my starting point when developing a ES6 based application using Webpack compiler, Babel for making it compliant with ES5 standard, Stylus/Sass/Less/CSS with an automatic vendor prefixer. It enables you to use [BrowserSync](https://browsersync.io/)

Whole setup is massivelly inspired by the [webpack.academy](https://webpack.academy) and then tuned to my specific needs. It isn't anywhere near perfect and I encourage you to improve on it and send me a pull request with an explanation of your improvements. Please do it!

## Configurations and corresponding scripts
The main webpack config is called traditionally `webpack.config.js` and decides which version of the configuration is going to be loaded by reading the value of the `env.env`. You have to supply this either via command line or in the scripts syntax in the package.json. You will find the partial configurations in the `./build-utils` folder.

`"build:dev": "yarn build --env.env=dev"` - for the development build
`"build:prod": "yarn build --env.env=prod"` - for the production build

Run them using yarn (my weapon of choice, but you can change it to npm): `yarn build:dev` or `yarn build:prod`

In case you want webpack to recompile every time you make a change to one of the files, use the _watch_ scripts: `yarn build:watch:dev` or `yarn build:watch:prod`

Alternativelly you can have the browser refresh and your application recompiled at the same time if you use the BrowserSync script `yarn build:browsersync`

### Common Configuration
These settings are included in all builds. The `./build-utils/common-paths.js` makes common paths used across the setup accessible to any config file that imports it. The `./build-utils/webpack.common.js` contains setting that are to be used in any environment. This gets loaded before the environment configs.

### Development Configuration
Development config compiles all the styles into the `./dist/bundle.js` as in the dev mode I don't really care about its size. You can use any of the common style pre-processors – Stylus (I really got to like this one), Sass, Less or pure CSS. The source maps don't always work correctly, but their close enough for me at this stage. If you have time and will to find out, how to improve on them. Please send me a pull request with your solution.

### Production Configuration
Production config is a bit more advanced. I extract any compiled CSS (Stylus, Sass, Less) into a separate CSS file as many web apps expect that (Wordpress, Drupal). I also uglify JS and compress all output.

### Addons Configuration
What I really like about this setup is its flexibility. If you want to add any extra plugins you can just make separate config files for them in the `./build-utils/addons` folder and then invoke these in your script (or CLI command) using the --env.addons variable. See the example of the BrowserSync script: `"build:browsersync": "yarn build:watch:dev --env.addons=browsersync"` – neat!