const webpackMerge = require('webpack-merge');
const commonConfig = require('./build-utils/webpack.common');

const addonsFn = (addonsArg) => {
  const addons = [].concat
    .apply([], [addonsArg]) // Normalize array of addons (flatten)
    .filter(Boolean); // If addons is undefined, filter it out

  // eslint-disable-next-line global-require, import/no-dynamic-require
  return addons.map((addonName) => require(`./build-utils/addons/webpack.${addonName}.js`));
};

module.exports = (env) => {
  if (!env) {
    throw new Error(`You must pass an --env.env flag into your build for webpack to work!`);
  }
  console.log(env);

  // eslint-disable-next-line global-require, import/no-dynamic-require
  const envConfig = require(`./build-utils/webpack.${env.env}.js`);

  return webpackMerge(commonConfig, envConfig, ...addonsFn(env.addons));
};
