import Functions from './functions';
import './stylus/styles.styl';

const APP = {
  init() {
    console.log('Main App initiated!!!');
    // Start JS Functions
    Functions.init();
  },
};

APP.init();
