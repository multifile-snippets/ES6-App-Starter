// eslint-disable-next-line import/no-extraneous-dependencies
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

module.exports = {
  plugins: [
    new BrowserSyncPlugin({
      host: 'localhost',
      files: ['**/*.css', '**/*.html', '**/*.php'],
      proxy: 'example.com',
      https: true,
    }),
  ],
};
