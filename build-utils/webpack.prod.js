// eslint-disable-next-line import/no-extraneous-dependencies
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const extractStylePlugin = new ExtractTextPlugin({
  filename: `style.css`,
  // filename: `../dist/style.css`,
  allChunks: true,
});

// eslint-disable-next-line import/no-extraneous-dependencies
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const uglifyJS = new UglifyJsPlugin({
  test: /\.js($|\?)/,
  sourceMap: false,
  uglifyOptions: {
    // ecma: 6
    mangle: false,
    compress: false,
  },
});

// eslint-disable-next-line import/no-extraneous-dependencies
const CompressionWebpackPlugin = require('compression-webpack-plugin');

const compressAllPlugin = new CompressionWebpackPlugin({
  asset: '[path].gz[query]',
  algorithm: 'gzip',
  test: /\.(js|html|css)$/,
  threshold: 10240,
  minRatio: 0.8,
});

const config = {
  mode: 'production',
  // optimization: {
  //   splitChunks: {
  //     chunks: 'all'
  //   }
  // },
  module: {
    rules: [
      {
        test: /\.(scss|css|styl)$/,
        use: extractStylePlugin.extract({
          use: [
            {
              loader: 'css-loader',
              options: { importLoaders: 1 },
            },
            {
              loader: 'postcss-loader',
              // options: { sourceMap: true },
            },
            {
              loader: 'stylus-loader',
              // options: { sourceMap: true },
            },
            {
              loader: 'sass-loader',
              // options: { sourceMap: true },
            },
          ],
        }),
      },
    ],
  },
  plugins: [extractStylePlugin, uglifyJS, compressAllPlugin],
};

module.exports = config;
