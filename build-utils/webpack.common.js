// eslint-disable-next-line import/no-extraneous-dependencies
require('@babel/polyfill');
// eslint-disable-next-line import/no-extraneous-dependencies
const webpack = require('webpack');
const commonPaths = require('./common-paths');

const config = {
  entry: {
    main: ['@babel/polyfill', './src/app.js'],
  },
  output: {
    path: commonPaths.outputPath,
    filename: 'bundle.js',
    publicPath: '/dist/',
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env'],
              sourceMap: true,
            },
          },
        ],
      },

      {
        test: /\.(woff|woff2|eot|ttf|otf|svg)(\?v=\d+\.\d+\.\d+)?$/,
        use: [
          {
            loader: 'url-loader?limit=10000',
            // options: {
            //   name: `./fonts/[name].[ext]`
            // }
          },
        ],
      },

      {
        test: /\.(gif|jpg|jpeg|png)$/,
        use: [
          {
            loader: 'file-loader',
          },
        ],
      },
    ],
  },
  plugins: [new webpack.ProgressPlugin()],
};

module.exports = config;
