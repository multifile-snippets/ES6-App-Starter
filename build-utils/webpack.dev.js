const config = {
  mode: 'development',
  devtool: 'source-map', // http://webpack.js.org/configuration/devtool/
  // devServer: {
  //   // webpack.js.org/configuration/dev-server/
  //   contentBase: commonPaths.outputPath,
  //   compress: true,
  //   port: 9000
  // },
  module: {
    rules: [
      {
        test: /\.(scss|css|styl)$/,
        use: [
          {
            loader: 'style-loader',
            options: { sourceMap: true },
          },
          {
            loader: 'css-loader',
            options: { importLoaders: 1, sourceMap: true },
          },
          {
            loader: 'postcss-loader',
            options: { sourceMap: true },
          },
          {
            loader: 'stylus-loader',
            options: { sourceMap: true },
          },
          {
            loader: 'sass-loader',
            options: { sourceMap: true },
          },
        ],
      },
    ],
  },
  // plugins: []
};

module.exports = config;
